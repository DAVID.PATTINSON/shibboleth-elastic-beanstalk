"""
Provides a dummy WSGI application for gunicorn.

Elastic Beanstalk requires a "web" proc to be running otherwise deployments and health
checks fail. Since the Apache proxy is being used as the WSGI server, gunicorn is
not needed. This dummy WSGI application allows gunicorn to run without interfering
with Django.
"""


def application(environ, start_response):
    """Source: https://go.wisc.edu/2b0qt9"""
    status = "200 OK"
    output = b"Dummy WSGI application. This should not be visible outside the server."

    response_headers = [
        ("Content-type", "text/plain"),
        ("Content-Length", str(len(output))),
    ]
    start_response(status, response_headers)

    return [output]
