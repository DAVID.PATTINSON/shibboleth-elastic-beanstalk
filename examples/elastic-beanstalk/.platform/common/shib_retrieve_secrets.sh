#!/usr/bin/env bash
# Retrieves the Shibboleth Secrets from Secrets Manager.
# Expects the secret to be a JSON object with keys = filename and values = content
#   {
#      "sp-encrypt-cert.pem": "-----BEGIN CERTIFICATE----- ...",
#      "sp-signing-cert.pem": "-----BEGIN CERTIFICATE----- ...",
#      ...
#   }

set -e

if [[ -x ${SHIB_SECRETS_ARN} ]]; then
  echo "ERROR: SHIB_SECRETS_ARN environment variable is not defined! Aborting shib setup..."
  exit 1
fi

if [[ -x ${SHIB_SECRETS_REGION} ]]; then
  echo "ERROR: SHIB_SECRETS_REGION environment variable is not defined! Aborting shib setup..."
  exit 1
fi

echo "Retrieving Shibboleth secrets from ${SHIB_SECRETS_ARN}..."
secrets=$(aws --region "${SHIB_SECRETS_REGION}" --output json secretsmanager get-secret-value --secret-id \
  "${SHIB_SECRETS_ARN}" | jq '.["SecretString"]' --raw-output)
filenames=$(echo "$secrets" | jq --raw-output "keys[]")

for filename in $filenames; do
  if ! secret=$(echo "$secrets" | jq --raw-output ".[\"${filename}\"]"); then
    echo "ERROR: Unable to retrieve secret for ${filename}. Aborting..."
    exit 1
  fi
  filepath="/etc/shibboleth/${filename}"
  echo "$secret" > "${filepath}"
  echo "   Wrote ${filepath}"
done

chmod 640 /etc/shibboleth/*.pem
chown shibd:shibd /etc/shibboleth/*.pem
