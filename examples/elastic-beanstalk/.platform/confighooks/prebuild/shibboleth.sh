#!/usr/bin/env bash
# Updates Shibboleth configuration in case the related environment variables are changed

set -e

source ".platform/common/shib_retrieve_secrets.sh"
source ".platform/common/shib_restart.sh"
