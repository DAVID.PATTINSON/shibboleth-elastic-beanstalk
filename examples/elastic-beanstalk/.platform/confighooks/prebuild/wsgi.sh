#!/usr/bin/env bash

set -e

# Pass environment variables set by Elastic Beanstalk when systemd starts Apache
systemctl import-environment
