#!/usr/bin/env bash
# Installs shibboleth and retrieves secrets

set -e

echo "Checking if Shibboleth is installed..."
if ! yum list installed shibboleth; then
  echo "Installing shibboleth..."
  curl http://download.opensuse.org/repositories/security://shibboleth/CentOS_7/security:shibboleth.repo -o /etc/yum.repos.d/shibboleth.repo
  yum -y install shibboleth.x86_64

  # Remove pre-generated keys and certs
  rm /etc/shibboleth/*.pem
else
  echo "Shibboleth is already installed"
fi

source .platform/common/shib_copy_config.sh
source .platform/common/shib_retrieve_secrets.sh
source .platform/common/shib_restart.sh
