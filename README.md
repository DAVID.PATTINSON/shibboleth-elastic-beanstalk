# Shibboleth in Elastic Beanstalk

This repository contains a guide to configure Shibboleth Service Provider (SP) within an AWS Elastic Beanstalk (EB) environment.
The goals of the presented design are:

- Provide a Shibboleth session to the web application
- Automate the installation and configuration of Shibboleth SP within EC2 instances
- Support auto-scaling
- Follow recommended security and configuration practices

While this guide is specific to Elastic Beanstalk, the strategy of automating Shibboleth SP installation and configuration
is applicable in any hosted environment. This guide and scripting examples will be useful outside the context of EB.

## Table of Contents

[Prerequisite Knowledge](docs/prerequisites.md)

[Architecture Overview](docs/architecture-overview.md)

[Elastic Beanstalk Configuration](examples/elastic-beanstalk)

[Secure Storage of Shibboleth Secrets](docs/shib-secrets.md)

## Questions or Suggestions?

[Submit an issue!](https://git.doit.wisc.edu/wps-public/knowledge-sharing-documentation/cloud-solutions/shibboleth-elastic-beanstalk/-/issues/new)

## Contributing

Merge requests are welcome. Please see the [Contributing Guide](CONTRIBUTING.md)

## Disclaimer

This design has not been reviewed by UW-Madison Cybersecurity. Carefully read this design and its accompanying scripts before implementation. Use at your own risk!
