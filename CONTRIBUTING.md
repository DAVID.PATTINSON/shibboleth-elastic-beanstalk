# Contribution Guide

Merge requests are welcome! Please use the [Forking Workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/forking-workflow).

## Formatting Guidelines

- Consider using [Prettier](https://www.google.com/search?client=firefox-b-1-d&q=prettier) and [Beautysh](https://pypi.org/project/beautysh/) for consistent formatting.
- Indents should be 2 spaces.
- Prose should not be wrapped.

## Large Contributions

If you want to make a large contribution, consider [opening an issue](https://git.doit.wisc.edu/wps-public/knowledge-sharing-documentation/cloud-solutions/shibboleth-elastic-beanstalk/-/issues/new) to plan with the maintainers.
