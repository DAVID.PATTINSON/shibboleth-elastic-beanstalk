# Architecture Overview

```mermaid
graph LR
    request([Web Request]) --> alb
    subgraph aws [Elastic Beanstalk]
        alb[EC2 Application Load Balancer] -->|https| httpd[Apache Proxy Server]
        subgraph ec2 [EC2 Instance]
            shibd[Shibboleth SP] --- httpd
            httpd -->|shib session| app([Web Application])
        end
        sm[(Secrets Manager)] -->|secrets| shibd
    end
    shibd ---|https| idp[Shibboleth IdP]
    style idp stroke-dasharray: 5 5
    style aws fill:none
```

**Diagram:** The customized Elastic Beanstalk environment to support Shibboleth login
