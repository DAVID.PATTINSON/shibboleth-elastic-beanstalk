# Prerequisite Knowledge

This guide will not cover the following topics, assuming that the reader has an understanding of them.
One does not need to be an expert, however. This guide provides configuration suggestions, automated scripts, and
links to related documentation.

This guide was written for developers within [University of Wisconsin-Madison](https://www.wisc.edu). If you are visiting from elsewhere,
welcome! This guide is designed to be useful to any Shibboleth environment. However, some suggestions and links may only
be applicable to consumers of the [UW-Madison NetID Login Service](https://kb.wisc.edu/86317)

## Shibboleth Service Provider

- [Shibboleth Concepts](https://wiki.shibboleth.net/confluence/display/CONCEPT/Home)
- [Shibboleth SP Installation](https://wiki.shibboleth.net/confluence/display/SP3/LinuxInstall)
- [Shibboleth SP Configuration](https://wiki.shibboleth.net/confluence/display/SP3/ConfigurationFileSummary) - in particular, `shibboleth2.xml`
- [NetID Login Service - Shibboleth Service Provider Configuration File](https://kb.wisc.edu/86698)

## Amazon Web Services

- [AWS Management Console](https://aws.amazon.com/console/)
- [AWS Command Line Interface (CLI)](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-welcome.html) - installed
  and [configured with credentials](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html)
- [Elastic Beanstalk](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/Welcome.html)
  - [EB CLI](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/eb-cli3.html) - installed
  - [Elastic Beanstalk Configuration](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/customize-containers.html)
  - [Elastic Beanstalk Platform Hooks](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/platforms-linux-extend.html)
- [AWS Secrets Manager](https://docs.aws.amazon.com/secretsmanager/latest/userguide/intro.html)
- [AWS Identity and Access Management (IAM)](https://docs.aws.amazon.com/IAM/latest/UserGuide/introduction.html)
- [AWS Elastic Compute Cloud (EC2)](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/concepts.html)

## Other Knowledge

- Linux
- Bash CLI and scripting
- Apache web server configuration
  - HTTPS and SSL certificates

## Time and Patience

You are about to conduct a symphony of technologies, each with their own personalities and configuration. Take the time
to understand these technologies before using provided examples. Be prepared to break your environment, inspect logs,
and solve a series of problems as you stand up Shibboleth login.

> "Do one thing every day that scares you." ― Eleanor Roosevelt
